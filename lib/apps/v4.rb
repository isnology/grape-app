module API
  class AppV4 < Grape::API
    version :v4, using: :path

    format :json

    helpers do
      def log
        AppV4.logger
      end
    end

    before do
      log.warn("Parameters: #{params}")
      log.warn("Route info: #{route}")
    end

    get '/' do
      "Thank You."
    end
  end
end