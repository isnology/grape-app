module API
  class AppV3 < Grape::API

    version :v3, using: :path

    format :json

    resource :files do
      get '/:id/download' do
        file = Asset[params[:id]]
        if file.can_be_downloaded_with?(params[:receiver])
          path = file.file_url.gsub('public/', '')\

          redirect path
        else
          error!("You're not allowed to download this file.", 403)
        end
      end
    end
  end
end
