require 'rspec'
require 'env'
require 'apps/v3'
require 'uploaders/asset'
require 'models/asset'

describe  API::AppV3 do
  include Rack::Test::Methods

  def app
    API::AppV3
  end

  let(:id) { 331 }
  let(:path) { "/v3/files/#{id}/download" }

  it 'downloads a file as another user' do
    get path, receiver: 'net@tutsplus.com'

    expect(last_response.status).to eq(302)
  end

  it 'fails to download the file as the incorrect user' do
    get path, receiver: 'glenn@tutsplus.com'

    expect(last_response.status).to eq(403) # Forbidden
  end
end