require 'rspec'
require 'env'
require 'apps/v2'
require 'uploaders/asset'
require 'models/asset'

describe API::AppV2 do
  context 'Download a file' do
    include Rack::Test::Methods

    def app
      API::AppV2
    end

    it 'downloads a file' do
      authorize('glennmarks', 'password')
      asset = Asset.last
      get "/v2/files/#{asset.id}/download"

      expect(last_response.status).to eq(302)
    end
  end
end
