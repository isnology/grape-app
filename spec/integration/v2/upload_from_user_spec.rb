require 'rspec'
require 'uploaders/asset'
require 'models/asset'
require 'apps/v2'

describe API::AppV2 do
  context 'Upload a file with authorised user' do

    include Rack::Test::Methods

    def app
      API::AppV2
    end

    context 'when authorized' do

      before do
        file_path = fixture_path('zip.zip')

        authorize 'glennmarks', 'password' # basic http authorization
        post '/v2/files', { file: { title: 'My First Zip File' ,
                                 file: Rack::Test::UploadedFile.new(file_path, 'application/zip', true)
                     }
                     }
      end

      it 'uploads a file' do
        expect(last_response.status).to eq(201)
      end

      it 'retrieves content for the new file' do
        expect(last_response.body).to include('My First Zip File')
      end

      it 'retrieves the actual filename' do
        expect(last_response.body).to include('zip.zip')
      end

      it 'retrieves the full path of the file' do
        #require 'pry'; binding.pry
        expect(last_response.body).to  include('public/uploads/assets')
      end

      it 'references the user from the file' do
        expect(last_response.body).to include('glennmarks')
      end

    end

    context 'when not authorized'do

      it 'fails if no authentication is provided' do
        post '/v2/files'

        expect(last_response.status).to eq(401) # Unauthorized
      end
    end

  end
end
