require 'rspec'
require 'env'
require 'apps/v2'
require 'uploaders/asset'
require 'models/asset'

describe API::AppV2 do
  context 'Retieving a single file' do
    include Rack::Test::Methods

    def app
      API::AppV2
    end

    it 'retrieves a file' do
      authorize 'glennmarks', 'password'
      get '/v2/files/331'

      expect(last_response.body).to include('My First Zip File')
    end

    it 'complains about a bad id' do
      authorize 'glennmarks', 'password'
      get '/v2/files/zip.zip'

      expect(last_response.status).to eq(404) # should be 400 ???
    end
  end
end
