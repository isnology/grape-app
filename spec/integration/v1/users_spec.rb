require 'rspec'
require 'models/user'

describe API::App do
  context 'User' do
    include Rack::Test::Methods

    def app
      API::App
    end

    describe 'signing up' do
      before do
        post '/users', user: { handler:  'glennmarks',
                             name:     'Glenn Marks',
                             password: 'password' }
      end

      it 'creates a user' do
        expect(last_response.status).to eq(201) #created
      end

      it 'shows the handler' do
        expect(last_response.body).to include('glennmarks')
      end

      it 'shows the name' do
        expect(last_response.body).to include('Glenn Marks')
      end
    end
  end
end
