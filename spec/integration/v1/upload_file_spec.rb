require 'rspec'
require 'uploaders/asset'
require 'models/asset'
require 'app'

describe API::App do
  context 'Upload a file' do

    include Rack::Test::Methods

    def app
      API::App
    end

    before do
      file_path = fixture_path('zip.zip')
      post '/files', { file: { title: 'My First Zip File' ,
                               file: Rack::Test::UploadedFile.new(file_path, 'application/zip', true)
                             }
                     }
    end

    it 'uploads a file' do
      expect(last_response.status).to eq(201)
    end

    it 'retrieves content for the new file' do
      expect(last_response.body).to include('My First Zip File')
    end

    it 'retrieves the actual filename' do
      expect(last_response.body).to include('zip.zip')
    end

    it 'retrieves the full path of the file' do
      #require 'pry'; binding.pry
      expect(last_response.body).to  include('public/uploads/assets')
    end
  end
end
