require 'rspec'
require 'uploaders/asset'
require 'models/asset'
require 'app'

describe API::App do
  context 'Listing all files' do
    include Rack::Test::Methods

    def app
      API::App
    end

    before do
      get '/files'
    end

    it 'provides a valid response' do
      expect(last_response.status).to eq(200)
    end

    it 'lists all files' do
      expect(last_response.body).to include('My First Zip File')
    end
  end
end
